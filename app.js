// Cargar modulos y crear nueva aplicacion
var express = require("express"); 
var app = express();
var bodyParser = require('body-parser');
var fs = require("fs");
app.use(bodyParser.json()); // soporte para bodies codificados en jsonsupport
app.use(bodyParser.urlencoded({ extended: true })); // soporte para bodies codificados

//Ejemplo: getUser http://localhost:8080/getUser/:username
app.get('/getUser/:username', (req, res) => {
    fs.readFile( __dirname + "/" + "users.json", 'utf8', (err, data) => {
        data = JSON.parse(data);
        var response = data.find(user => user.username === req.params.username);

        if ( response === undefined) {
            response = {"respuesta": "Usuario inexistente"}
        }
        res.send(response);
    });
});

//Ejemplo: POST http://localhost:8080/activateUser
app.post('/activateUser', (req, res) => {

    fs.readFile( __dirname + "/" + "users.json", 'utf8', (err, data) => {
        users = JSON.parse(data);
        var user = users.find(user => user.username === req.body.username);

        response = {"respuesta": "status_code = 0"}
        if ( user === undefined) {
            response = {"respuesta": "Usuario inexistente"}
        } else {
            user.active = true;
            users.forEach((element, index) => {
                if(element.username === req.body.username) {
                    users[index] = user;
                }
            });
            fs.writeFileSync('users.json', JSON.stringify(users));
        }
        console.log( users );

        res.send(response);
    });

});

//Ejemplo: POST http://localhost:8080/deactivateUser
app.post('/deactivateUser', (req, res) => {

    fs.readFile( __dirname + "/" + "users.json", 'utf8', (err, data) => {
        users = JSON.parse(data);
        var user = users.find(user => user.username === req.body.username);

        response = {"respuesta": "status_code = 0"}
        if ( user === undefined) {
            response = {"respuesta": "Usuario inexistente"}
        } else {
            user.active = false;
            users.forEach((element, index) => {
                if(element.username === req.body.username) {
                    users[index] = user;
                }
            });
            fs.writeFileSync('users.json', JSON.stringify(users));
        }
        console.log( users );

        res.send(response);
    });

});

//Ejemplo: PUT http://localhost:8080/addUser
app.put('/addUser', (req, res) => {

    fs.readFile( __dirname + "/" + "users.json", 'utf8', (err, data)  => {
        users = JSON.parse(data);
        users.push(req.body);
        console.log( users );
        fs.writeFileSync('users.json', JSON.stringify(users));  
        response = {"respuesta": "status_code = 0"}
        res.send(response);
    });

});
  
var server = app.listen(8080, () => {
    console.log('Server is running..'); 
});